use Geuse::Preview::HTTP;

unit module Geuse::Preview::LiveReload;

our sub reload-browser(
    Int:D :$port,
    Str:D :$host,
    Bool  :$no-livereload = False
    --> Bool()
) {
    unless $no-livereload {
        my $request = "GET /reload HTTP/1.0\r\nContent-length: 0\r\n\r\n";
        HTTP.new(:$port, :$host, :$no-livereload).inet-request($request);
    }
}

