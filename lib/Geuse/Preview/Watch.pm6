use Geuse::Preview::HTTP;
use Geuse::Preview::LiveReload;
use File::Find;

unit class Geuse::Preview::Watch;

has $.writer;
has $.config;
has Int $.port;
has Str $.host;
has Bool $.no-livereload;

method build-and-reload( --> Bool ) {
    $!writer.write;
    Geuse::Preview::LiveReload::reload-browser(:$!port, :$!host, :$!no-livereload);
}

method user-input( Array :$servers --> Bool ) {
    loop {
        print q:to/USER-PROMPT/;
        `r enter` to [rebuild]
        `c enter` to [clear] build directory and rebuild
        `q enter` to [quit]
        USER-PROMPT

        given prompt('') {
            when 'r' {
                put "Rebuild triggered";
                self.build-and-reload;
            }
            when 'c' {
                put "Clear build directory and rebuild triggered";
                #Uzu::Render::clear %config;
                self.build-and-reload;
            }
            when 'q'|'quit' {
                exit 1;
            }
        }
    }
}

method start( --> Bool ) {
    # Initialize build
    put "Initial build";
    $!writer.write;
   
    # Track time delta between File events. 
    # Some editors trigger more than one event per edit. 
    my List $exts = ('css', 'md', 'html', 'yml', 'mustache');#%config<extensions>;
    my List $dirs = (
        |$!config.directories.grep({$_.key ne 'output'}).hash.values
    ).grep(*.IO.e).List;

    $dirs.map(-> $dir {
        put "Starting watch on {$dir.subst("{$*CWD}/", '')}"
    });

    # Start server
    my @servers = HTTP.new(
        :build-dir($!config.directories<output>),
        :$!port,
        :$!host,
        :$!no-livereload,
        :!omit-html-ext
    ).webserver;

    # Keep track of the last render timestamp
    state Instant $last_run = now;

    # Watch directories for modifications
    start {
        react {
            whenever self!file-change-monitor($dirs) -> $e {
                # Make sure the file change is a 
                # known extension; don't re-render too fast
                if so $exts (cont) $e.path.IO.extension and (now - $last_run) > 2 {
                    put "Change detected [{$e.path()}]";
                    self.build-and-reload();
                    $last_run = now;
                }
            }
        }
    }

    # Listen for keyboard input
    self.user-input(:@servers);
}

# PRIVATE METHODS

method !find-dirs( IO::Path $p --> Slip ) {
    slip ($p.IO, slip find :dir($p.path), :type<dir>);
}

method !file-change-monitor( List $dirs --> Supply ) {
    supply {
        my &watch-dir = -> $p {
            whenever IO::Notification.watch-path($p.path) -> $c {
                if $c.event ~~ FileRenamed && $c.path.IO ~~ :d {
                    self!find-dirs($c.path).map(watch-dir $_);
                }
                emit $c;
            }
        }
        #for $dirs.map({ self!find-dirs($_.IO) }) {
        #    watch-dir($_.path.Str) 
        #}
        watch-dir(~$_.path.Str) for $dirs.map: { self!find-dirs($_.IO) };
    }
}

