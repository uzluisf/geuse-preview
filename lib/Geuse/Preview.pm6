use Geuse::Preview::Watch;
use Geuse::Preview::HTTP;

unit module Geuse::Preview;

#| Entry point that Geuse uses for registering the module.
our sub register( $context is rw, %plugin-config --> Nil ) {
    my %dirs = $context.config.directories;
    my @dirs = ($_<content>, $_<templates>, $_<include>) given %dirs;

    my $build-dir = $context.config.directories<output>; 
    my $port      = %plugin-config<port> // 3000;
    my $host      = %plugin-config<host> // '0.0.0.0';

    # add 'watch' command
    $context.command.add: 
        capture => \('watch'),
        routine => sub ('watch') {
            Watch.new(
               :writer($context.writer),
               :config($context.config),
               :$port,
               :$host,
               :!no-livereload
            ).start()
        },
        description => 'Start web server and re-render site'
    ;
   
    # add 'webserver' command
    $context.command.add: 
        capture => \('serve'),
        routine => sub ('serve') { 
            await HTTP.new(
                :$build-dir,
                :$port,
                :$host,
                :no-livereload,
                omit-html-ext => False,
            ).webserver;
        },
        description => 'Start local web server'
    ;
}
