=begin pod

=head1 Name

C«Geuse::Preview» - Add local previewing support to the static
site generator L«Geuse|https://gitlab.com/uzluisf/geuse».

=head1 Description

This module provides a development server so that you can test your
modifications as you work. It can be enabled as follows

=begin code
plugins:
    - name: 'Geuse::Preview'
      host: '0.0.0.0'
      port: 3000
=end code

and it adds the following two commands to C«geuse»:

=begin code
geuse serve
    start local web server
geuse watch 
    start web server and re-render site
=end code

For more information on how Geuse picks up a plugin, look at
Geuse's L«documentation|https://uzluisf.gitlab.io/geusedoc/».

=head1 Installation

=head2 Using zef:
=code zef update && zef install Geuse::Preview

=head2 From source:
=begin code
git clone git@gitlab.com:uzluisf/geuse-preview.git
cd geuse-preview && zef install .
=end code

=head1 Dependencies

This modules depends on

=item C«HTTP::Server::Tiny»
=item C«File::Find»

You can install them by using the module manager C«zef».

=head1 Acknowledgement

C«Geuse::Preview::HTTP», C«Geuse::Preview::Watch» and
C«Geuse::Preview::LiveReload» were borrow-adapted from
L«Uzu|https://github.com/scmorrison/uzu». All the credit to Sam Morrison.
=end pod
