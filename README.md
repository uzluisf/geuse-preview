Name
====

`Geuse::Preview` - Add local previewing support to the static site generator [Geuse](https://gitlab.com/uzluisf/geuse).

Description
===========

This module provides a development server so that you can test your modifications as you work. It can be enabled as follows

    plugins:
        - name: 'Geuse::Preview'
          host: '0.0.0.0'
          port: 3000

and it adds the following two commands to `geuse`:

    geuse serve
        start local web server
    geuse watch 
        start web server and re-render site

For more information on how Geuse picks up a plugin, look at Geuse's [documentation](https://uzluisf.gitlab.io/geusedoc/).

Installation
============

Using zef:
----------

    zef update && zef install Geuse::Preview

From source:
------------

    git clone git@gitlab.com:uzluisf/geuse-preview.git
    cd geuse-preview && zef install .

Dependencies
============

This modules depends on

  * `HTTP::Server::Tiny`

  * `File::Find`

You can install them by using the module manager `zef`.

Acknowledgement
===============

`Geuse::Preview::HTTP`, `Geuse::Preview::Watch` and `Geuse::Preview::LiveReload` were borrow-adapted from [Uzu](https://github.com/scmorrison/uzu). All the credit to Sam Morrison.

